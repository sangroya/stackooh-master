
/****************************************************/

/* Annyang  */

/****************************************************/

	var isSpeechOn = false;

    if (annyang) {

	    const panelCommands = {
	        'show me *tag': changeTab,
	        'open tab *tag' : openTab,
	        'create page *tag' : createPage
	    };


	    const menuCommands = {
	        'add new link *tag' : addNewMenuItem, 
	        'delete link *tag' : deleteMenuItem,	        
	    };


	    const inspectorCommands = {

	    }


	    const bottomBarCommands = {
	    	'save project' : saveProject,
	    	'publish project' : publishProject,
	    	'download project' : downloadProject,
	    	'preview project' : previewProject
	    }


	    const siteCommands = {
	    	'change *tag to *tag2': updateText, //Change text on page
	    	'update *tag to *tag2': copyChange, 
	    	'add to *tag insert *tag2': appendToCopy

	    }


          // Add our commands to annyang
	      annyang.addCommands(panelCommands);
	      annyang.addCommands(menuCommands);
	      annyang.addCommands(siteCommands);
	      annyang.addCommands(bottomBarCommands);

    }



$(function() {

	$(document).on("click","#speechBtn",function() {
		if(isSpeechOn === false){
	     annyang.start({continuous: false });
	     $('.speech-circle').addClass('isActive').addClass('gradient-border');
	     $('.speech-circle-inner').addClass('isActive');
	     isSpeechOn = true;
		} else {
	      annyang.abort({continuous: false });
	     $('.speech-circle').removeClass('isActive').removeClass('gradient-border');
	     $('.speech-circle-inner').removeClass('isActive');
	     isSpeechOn = false;
		}
	});

});
    



/****************************************************/

/* Menu Commands  */

/****************************************************/


   function addNewMenuItem(tag) {
    console.log('addNewMenuItem: speech repeat', tag.toLowerCase());
    //Open menu panel
    $('.panel-nav-item[data-speech-item="menu"]').data('events',$('.panel-nav-item[data-speech-item="menu"]')).trigger("click");
    $('#create-new-menu-item').trigger("click");
    $('#selected-menu-name').val(tag);
    $('#add-new-menu-item').trigger("click");
  }

   function deleteMenuItem(tag) {
    console.log('deleteMenuItem: speech repeat', tag);
    //Open menu panel
    $('.panel-nav-item[data-speech-item="menu"]').data('events',$('.panel-nav-item[data-speech-item="menu"]')).trigger("click");
    $('[name=active-menu]').val(tag.replace(/^./, str => str.toUpperCase()) || tag.toUpperCase() || tag ).change();

    for (var i = 0; i < $("iframe").contents().find(".navbar-nav li").length; i++) { 
	  if($("iframe").contents().find(".navbar-nav li a")[i].innerText.toLowerCase() === tag.toLowerCase()){
          $("iframe").contents().find(".navbar-nav li")[i].remove();	  	
	  }
	}
  }


/****************************************************/

/* changeTab  */

/****************************************************/


  function changeTab(tag) {
    console.log('changeTab: speech repeat', tag.toLowerCase());
    $('.panel-nav-item[data-speech-item="'+tag+'"]').data('events',$('.panel-nav-item[data-speech-item="'+tag+'"]')).trigger("click");
  }


/****************************************************/

/* openTab  */

/****************************************************/

  function openTab(tag){
    console.log('openTab: speech repeat', tag.toLowerCase());
    $('.panel-nav-item[data-speech-item="elements"]').data('events',$('.panel-nav-item[data-speech-item="elements"]')).trigger("click");
     
      for (var i = 0; i < $( ".mat-expansion-panel-header" ).find( "span" ).children().length; i++) { 
		  if($( ".mat-expansion-panel-header" ).find( "span" ).children()[i].innerText.toLowerCase() === tag.toLowerCase()){
	        $( "#mat-expansion-panel-header-"+i).addClass('alerts-element').trigger("click");
		  }
	  }
	  removeAlert('.mat-expansion-panel-header');
  }

/****************************************************/

/* createPage  */

/****************************************************/

  function createPage(tag){
    console.log('createPage: speech repeat', tag.toLowerCase());
    
    //Set speech tag page name in window object 
    window.pageName = tag;
    
    //Trigger tab
    $('.panel-nav-item[data-speech-item="pages"]').data('events',$('.panel-nav-item[data-speech-item="pages"]')).trigger("click");
    
    //Click add new page button
    $('.new-page-button').trigger("click");

    //Trigger hidden select function
    $('#triggerOnPageSelected').trigger("click");

    //Click update button
    setTimeout(function(){ 
    //Update new value
    $('#selected-page-name').trigger("click").val(tag).trigger("change")
     }, 500);

  }

/****************************************************/

/* updateText  */

/****************************************************/

  function updateText(tag, tag2){
    console.log('updateText: tag speech repeat', tag.toLowerCase());
    console.log('updateText: tag2 speech repeat', tag2.toLowerCase());
   
   //Camalcase any space in string
   var camalize = tag.toLowerCase().trim().split(/[.\-_\s]/g).reduce((string, word) => string + word[0].toUpperCase() + word.slice(1));
   var elementOnPage = '#'+camalize;
   //Update Content on screen
   console.log('elementOnPage', elementOnPage);
   $("iframe").contents().find(elementOnPage).text(tag2);

   //$("iframe").contents().find("h1:contains("+tag+")").text(tag2)
   //$("iframe").contents().find("h1").text().replace('Landing Page', 'hgjjhfjf'); 
  }

/****************************************************/

/* copyChange  */

/****************************************************/

  function copyChange(tag, tag2){
    console.log('copyChange: tag speech repeat', tag.toLowerCase());
    console.log('copyChange: tag2 speech repeat', tag2.toLowerCase());

    //Get first and last word in string
    var firstWord = tag.replace(/ .*/,''); 
    var lastWord = tag.split(" ").pop();
    //word to num
    var mergedId = firstWord + text2num(lastWord)

   //Camalcase any space in string
   var camalize = mergedId.toLowerCase().trim().split(/[.\-_\s]/g).reduce((string, word) => string + word[0].toUpperCase() + word.slice(1));
   var elementOnPage = '#'+camalize;

   console.log('elementOnPage', elementOnPage)

   //Update Content on screen
   $("iframe").contents().find(elementOnPage).text(tag2+'.');

   }

/****************************************************/

/* appendToCopy  */

/****************************************************/

  function appendToCopy(tag, tag2){
  	console.log('appendToCopy: tag speech repeat', tag.toLowerCase());
    console.log('appendToCopy: tag2 speech repeat', tag2.toLowerCase());

     //Get first and last word in string
    var firstWord = tag.replace(/ .*/,''); 
    var lastWord = tag.split(" ").pop();
    //word to num
    if(text2num(lastWord) === 0){
     var mergedId = firstWord+lastWord;
    } else {
     var mergedId = firstWord + text2num(lastWord);
    }

    //Uppercase First letter
    const tagFirstCapitalized = tag2.charAt(0).toUpperCase() + tag2.slice(1)


   //Camalcase any space in string
    var camalize = mergedId.toLowerCase().trim().split(/[.\-_\s]/g).reduce((string, word) => string + word[0].toUpperCase() + word.slice(1));
    var elementOnPage = '#'+camalize;

    console.log('elementOnPage', elementOnPage)

   $("iframe").contents().find(elementOnPage).append(''+tagFirstCapitalized+'. ');

  } 


   function saveProject(){
    $('#saveProject').trigger("click");
   }

   function publishProject(){
    $('#publishProject').trigger("click");
  }

   function downloadProject(){
    $('#downloadProject').trigger("click");
  }

   function previewProject(){
    $('#previewProject').trigger("click");
  }

  

  /****************************************************/

/* Misc Function */

/****************************************************/


 function removeAlert(element){
 	setTimeout(function(){ 
 		//$(element).removeClass('alerts-element');
 		$(element+' alerts-element' ).fadeOut("slow", function() {
		    $(element).removeClass('alerts-element');
		});
 	}, 3000);
 }


  var Small = {
    'zero': 0,
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
    'ten': 10,
    'eleven': 11,
    'twelve': 12,
    'thirteen': 13,
    'fourteen': 14,
    'fifteen': 15,
    'sixteen': 16,
    'seventeen': 17,
    'eighteen': 18,
    'nineteen': 19,
    'twenty': 20,
    'thirty': 30,
    'forty': 40,
    'fifty': 50,
    'sixty': 60,
    'seventy': 70,
    'eighty': 80,
    'ninety': 90
};

var Magnitude = {
    'thousand':     1000,
    'million':      1000000,
    'billion':      1000000000,
    'trillion':     1000000000000,
    'quadrillion':  1000000000000000,
    'quintillion':  1000000000000000000,
    'sextillion':   1000000000000000000000,
    'septillion':   1000000000000000000000000,
    'octillion':    1000000000000000000000000000,
    'nonillion':    1000000000000000000000000000000,
    'decillion':    1000000000000000000000000000000000,
};

var a, n, g;

function text2num(s) {
    a = s.toString().split(/[\s-]+/);
    n = 0;
    g = 0;
    a.forEach(feach);
    return n + g;
}

function feach(w) {
    var x = Small[w];
    if (x != null) {
        g = g + x;
    }
    else if (w == "hundred") {
        g = g * 100;
    }
    else {
        x = Magnitude[w];
        if (x != null) {
            n = n + g * x
            g = 0;
        }
        else { 
            alert("Unknown number: "+w); 
        }
    }
}

const toCamelWord = (word, idx) =>
  idx === 0 ?
  word.toLowerCase() :
  word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();

const toCamelCase = text =>
  text
  .split(/[_-\s]+/)
  .map(toCamelWord)
  .join("");

//console.log(toCamelCase('User ID'))
